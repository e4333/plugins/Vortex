package me.jungo.vortex.game;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class GameTeamTest {
    @Test
    public void testOnDisable(){
        GameTeam team = new GameTeam("BLUE");
        Assertions.assertTrue(team.isActive());
    }

}
